#!/bin/bash

import os
import time
import threading


# Starts inactivity timer
def start_timer():
    platform = check_platform()
    if platform == "Windows":
        def timer_start():
            while True:
                time.sleep(1)
                check_if_idle_windows()
        thread1 = threading.Thread(target=timer_start)
        thread1.start()

    elif platform == "Linux":
        def timer_start():
            while True:
                time.sleep(1)
                check_if_idle_linux()
        thread1 = threading.Thread(target=timer_start)
        thread1.start()


# Checks what OS you're using
def check_platform():
    if os.name == "nt":
        platform = "Windows"
    else:
        platform = "Linux"
    return platform


# Checks if the user is idle on Windows
def check_if_idle_windows():
    import win32api
    idle_time = (win32api.GetTickCount() - win32api.GetLastInputInfo()) / 1000.0
    if idle_time > 3:
        os.system("cls")
        print("You have been logged out due to inactivity.")
        os._exit(0)


# Checks if the user is idle on Linux
def check_if_idle_linux():
    import subprocess
    idle_time = int(subprocess.getoutput('xprintidle')) / 1000 # Requires xprintidle (sudo apt install xprintidle)
    if idle_time > 3:
        os.system("clear")
        print("You have been logged out due to inactivity.")
        os._exit(0)


def random_function_for_main_thread():
    while True:
        my_string = input("Enter something or stay inactive for 3 seconds : ")
        print("You entered something.")


def main():
    start_timer()
    random_function_for_main_thread()


if __name__ == "__main__":
    main()
